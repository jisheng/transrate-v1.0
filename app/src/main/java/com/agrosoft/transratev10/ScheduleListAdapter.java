package com.agrosoft.transratev10;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;




public class ScheduleListAdapter extends RecyclerView.Adapter<ScheduleListAdapter.ViewHolder> {

    ArrayList<ScheduleListItem> ScheduleItems;
    SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    public ScheduleListAdapter() {
        String medicine = "Atenolol";
        Calendar now = Calendar.getInstance();
        ScheduleItems = new ArrayList<ScheduleListItem>();
        now.add(Calendar.SECOND, 60);
        ScheduleItems.add(new ScheduleListItem(medicine, false, now));
        
        for (int i = 0; i<10; i++) {
            Calendar future = Calendar.getInstance();
            future.set(Calendar.HOUR,9);
            future.set(Calendar.MINUTE,0);
            future.set(Calendar.SECOND, 0);
            future.set(Calendar.DAY_OF_MONTH, 14 + i);
            ScheduleItems.add(new ScheduleListItem(medicine, false, future));
        }
    }

    @Override
    public ScheduleListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_schedule, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ScheduleListItem m = ScheduleItems.get(position);

        // name
        holder.checkbox.setText(m.getName());
        holder.alarmclock.setText(df.format(m.getCalendar().getTime()));
        holder.checkbox.setChecked(m.getChecked());
        holder.v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m.setChecked(!m.getChecked());
            }
        });

    }

    @Override
    public int getItemCount() {
        return ScheduleItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        View v;
        CheckBox checkbox;
        TextView alarmclock;

        public ViewHolder(View v) {
            super(v);
            this.v = v;
            checkbox = (CheckBox) v.findViewById(R.id.checkbox);
            alarmclock = (TextView) v.findViewById(R.id.distance);
        }

    }

}