package com.agrosoft.transratev10;

import java.util.Calendar;

/**
 * Created by JiSheng on 6/14/15.
 */
public class ScheduleListItem {
    private String name;
    private Boolean checked;
    private Calendar calendar;

    public ScheduleListItem() {
        name = "";
        checked = false;
        calendar = Calendar.getInstance();
    }

    public ScheduleListItem(String name, Boolean checked, Calendar calendar) {
        this.name = name;
        this.checked = checked;
        this.calendar = calendar;
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean value) {
        this.checked = value;
    }

    public Calendar getCalendar(){
        return calendar;
    }

    public void setCalendar(Calendar value){
        this.calendar = value;
    }
}