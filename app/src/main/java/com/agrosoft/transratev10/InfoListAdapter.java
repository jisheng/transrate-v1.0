package com.agrosoft.transratev10;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class InfoListAdapter extends RecyclerView.Adapter<InfoListAdapter.ViewHolder> {

    ArrayList<String> infoItems;

    public InfoListAdapter() {
        infoItems = new ArrayList<String>();
        infoItems.add("Update");
        infoItems.add("Medical Record");
        infoItems.add("Schedule");
    }

    @Override
    public InfoListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_info, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final String m = infoItems.get(position);

        // name
        holder.textView.setText(m);
        holder.v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 0) {
                    Log.w("Pressed", "0");
                    Toast.makeText(v.getContext(), "You are running the latest version",
                            Toast.LENGTH_SHORT).show();
                } else if (position == 1) {
                    Log.w("Pressed", "1");
                    Intent intent = new Intent(v.getContext(), MedicalRecordActivity.class);
                    v.getContext().startActivity(intent);
                } else if (position == 2) {
                    Log.w("Pressed", "2");
                    Intent intent = new Intent(v.getContext(), ScheduleActivity.class);
                    v.getContext().startActivity(intent);
                }

            }
        });
        if (position == 0) {
            holder.imageView.setImageResource(R.mipmap.ic_update);
        } else if (position == 1) {
            holder.imageView.setImageResource(R.mipmap.ic_medical_records);
        } else if (position == 2) {
            holder.imageView.setImageResource(R.mipmap.ic_schedule);
        }

    }

    @Override
    public int getItemCount() {
        return infoItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        View v;
        TextView textView;
        ImageView imageView;

        public ViewHolder(View v) {
            super(v);
            this.v = v;
            textView = (TextView) v.findViewById(R.id.name);
            imageView = (ImageView) v.findViewById(R.id.listIcon);
        }

    }

}