package com.agrosoft.transratev10;

/**
 * Created by JiSheng on 6/14/15.
 */
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
//        Log.w("Hello", "onReceive called");
        NotificationManager mNotificationManager;
        int NOTIFICATION_ID = 1;
//        mNM = (NotificationManager)context.getSystemService(context.NOTIFICATION_SERVICE);
//        // Set the icon, scrolling text and timestamp
//        Notification notification = new Notification(R.mipmap.ic_launcher, "Test Alarm",
//                System.currentTimeMillis());
//        // The PendingIntent to launch our activity if the user selects this notification
//        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, new Intent(context, MainActivity.class), 0);
//        // Set the info for the views that show in the notification panel.
//        notification.setLatestEventInfo(context, context.getText(R.string.app_name), "This is a Test Alarm", contentIntent);
//        // Send the notification.
//        // We use a layout id because it is a unique number. We use it later to cancel.
//        mNM.notify(R.string.app_name, notification);
        mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                new Intent(context, MainActivity.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(context.getText(R.string.app_name))
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText("Time to eat medicine"))
                        .setContentText("Time to eat medicine");

        mBuilder.setDefaults(Notification.DEFAULT_ALL);
        mBuilder.setAutoCancel(true);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}