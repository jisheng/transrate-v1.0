package com.agrosoft.transratev10;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.app.Activity;
import android.content.Intent;
import android.media.Image;
import android.os.SystemClock;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.*;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.android.OpenCVLoader;
import org.opencv.ml.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.*;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends FragmentActivity implements CvCameraViewListener {

    static {
        if(!OpenCVLoader.initDebug()) {
            Log.i("opencv", "opencv initialization failed");
        } else {
            Log.i("opencv", "opencv initialization successful");
        }
    }

    private static final String TAG = "OCV:Activity";

    private CameraBridgeViewBase openCvCameraView;
    private CascadeClassifier cascadeClassifier;
    private Mat grayscaleImage;
    private int absoluteSize;
    private ImageButton playPauseButton;
    private ImageButton menuButton;
    private ImageButton learningButton;
    private boolean camerastatus;
    private boolean isLearning;
    private PendingIntent pendingIntent;
    private Mat imageRoi;
    private final int ATTRIBUTES = 256;
    private final int CLASSES = 26;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch(status) {
                case LoaderCallbackInterface.SUCCESS:
                    initializeOpenCVDependencies();
                    break;
                default:
                    super.onManagerConnected(status);
                    break;
            }
        }
    };

    private void initializeOpenCVDependencies() {
        try {
            // Copy the resources into a temp file so OpenCV can load it
            InputStream is = getResources().openRawResource(R.raw.myfacedetector);
            File cascadeDir = getDir("cascade", Context.MODE_APPEND);
            File mCascadeFile = new File(cascadeDir, "myfacedetector.xml");
            FileOutputStream os = new FileOutputStream(mCascadeFile);

            byte[] buffer = new byte[4096];
            int bytesRead;
            while((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.close();

            // Load the cascade classifier
            cascadeClassifier = new CascadeClassifier(mCascadeFile.getAbsolutePath());

        }catch(Exception e) {
            Log.e("OpenCVActivity", "Error loading cascade", e);
        }

        BufferedReader reader = null;
        LinkedList list = new LinkedList();
        LinkedList listInfo = new LinkedList();
        try {
            reader = new BufferedReader(new InputStreamReader(getAssets().open("transmed_data.txt")));

            String line = reader.readLine();
            list.add(line);
            while(line != null) {
                line = reader.readLine();
                if(line == "")
                    break;

                list.add(line);
            }

            Iterator it = list.iterator();
            while(it.hasNext()) {
                String str = (String)it.next();
                switch(str) {
                    case "atenolol":
                        listInfo.add("Atenolol");
                        break;
                    default:
                        break;
                }
                // Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
            }
        } catch(Exception e) {
            Log.e("OpenCVActivity", "Error loading transmed_data", e);
        } finally {
            if(reader != null) {
                try {
                    reader.close();
                } catch(Exception e) {}
            }
        }

        try {
            CvANN_MLP nnetwork;

        } catch(Exception e) {
            Log.e("OpenCVActivity", "Error loading xml", e);
        }

        openCvCameraView.enableView();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, " called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_main);
        openCvCameraView = (CameraBridgeViewBase)findViewById(R.id.java_surface_view);
        openCvCameraView.setCvCameraViewListener(this);
        camerastatus = true;
        isLearning = false;
        learningButton = (ImageButton) findViewById(R.id.learningButton);
        learningButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLearning) {
                    //I am not learning
                    isLearning = false;
                    learningButton.setImageResource(R.mipmap.ic_focus);
                    Toast.makeText(getApplicationContext(), "Scanning Mode", Toast.LENGTH_SHORT).show();
                } else {
                    //I am not learning
                    isLearning = true;
                    learningButton.setImageResource(R.mipmap.ic_learning);
                    Toast.makeText(getApplicationContext(), "Learning Mode", Toast.LENGTH_SHORT).show();
                }
            }
        });
        playPauseButton = (ImageButton) findViewById(R.id.playPauseButton);
        playPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (camerastatus) {
                    openCvCameraView.disableView();
                    camerastatus = false;
                    playPauseButton.setImageResource(R.mipmap.ic_play);
                } else {
                    openCvCameraView.enableView();
                    camerastatus = true;
                    playPauseButton.setImageResource(R.mipmap.ic_pause);
                }
            }
        });
        menuButton = (ImageButton) findViewById(R.id.menuButton);
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RecyclerViewDialog dialog = new RecyclerViewDialog();
                DialogFragment fragment = dialog;
                fragment.show(getSupportFragmentManager(), "dialog");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        grayscaleImage = new Mat(height, width, CvType.CV_8UC4);
        absoluteSize = (int)(height * 0.2);
    }

    @Override
    public void onCameraViewStopped() {}

    @Override
    public Mat onCameraFrame(Mat frame) {
        // Create a grayscale image
        Imgproc.cvtColor(frame, grayscaleImage, Imgproc.COLOR_RGBA2BGR);
        MatOfRect character = new MatOfRect();
        float pixelValueArray[] = new float[256];
        Mat data = new Mat(1, ATTRIBUTES, CvType.CV_32F);
        Mat image = new Mat();
        Mat output = new Mat();

        // Use the classifier for detection
        if(cascadeClassifier != null) {
            cascadeClassifier.detectMultiScale(grayscaleImage, character, 1.1, 2, 2,
                    new Size(absoluteSize, absoluteSize), new Size());
        }

        // If there any object found, draw a rectangle around it
        Rect rectCrop = null;
        for(Rect rect : character.toArray()) {
            Core.rectangle(frame,new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(0, 255, 0, 255), 3);

            rectCrop = new Rect(rect.x, rect.y, rect.width, rect.height);
            imageRoi = new Mat(grayscaleImage, rectCrop);

            image = imageRoi;

            Imgproc.GaussianBlur(image, output, new Size(5,5), 0);
            Imgproc.threshold(output, output, 50, 255, 0);

            Size size = new Size(16, 16);
            Imgproc.resize(output, output, size);

            // pixel binary covert
        }

        return frame;
    }

    @Override
    public void onPause() {
        super.onPause();
        if(openCvCameraView != null)
            openCvCameraView.disableView();
    }

    @Override
    public void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_9, this, mLoaderCallback);
    }

    public void onDestroy() {
        super.onDestroy();
        if(openCvCameraView != null)
            openCvCameraView.disableView();
    }

    public static class RecyclerViewDialog extends DialogFragment {

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
            View v = inflater.inflate(R.layout.dialog_material_recyclerview, container, false);
            final RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.list);
            final InfoListAdapter mInfoAdapter = new InfoListAdapter();
            LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLinearLayoutManager);
            recyclerView.setAdapter(mInfoAdapter);


            recyclerView.post(new Runnable() {
                public void run() {
                    int height = (int) (mInfoAdapter.getItemCount() * (53 * getResources().getDisplayMetrics().density));
                    if (recyclerView.getHeight() > height) {
                        ViewGroup.LayoutParams param = recyclerView.getLayoutParams();
                        param.height = height;
                        recyclerView.setLayoutParams(param);
                    }
                }
            });
            return v;
        }

    }
}
